This installation manual assumes that your local machine and the server are both Debian (for instance Ubuntu) machines.

**Local dependencies:**

fabric, fabtools


```
#!bash

sudo pip install Fabric fabtools
```

**Server dependencies:**

You will need a user with sudo privileges.
(Tested on Ubuntu Server 14.04.1)
Before going further, update the system:

```
#!bash

sudo apt-get update && sudo apt-get upgrade

sudo reboot
```

** On your local machine **

Edit fabricrc file in the root of the project folder. It contains all the settings. You should not have to edit all of them. In most cases the defaults are fine. There are additional instructions within fabricrc file.

In fact, you need only 3 files on your local machine:

**fabfile.py** — installation file;

**fabricrc** — settings file;

**deploy.sh** — a wrapper.

In order to start the installation:

```
#!bash

./deploy.sh setup

```

In order to update:

```
#!python

./deploy.sh update

```

The deploy.sh contains additional commands:

start, stop, restart, status

That is it!

The default admin account: admin@example.com 
Password: admin

If you want to update any of the values in fabricrc, update them locally and execute:

```
#!bash

./deploy.sh update

```

The system will be updated and restarted.

These values in most cases should remain unchanged:

**mongodb_user, mongodb_passwd, mongodb_name, mongodb_host, mongodb_port**

If you want to run the system locally, you can find **dot_env.example** in **settings** folder.